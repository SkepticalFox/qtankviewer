#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "utils/vechicleinfo.h"


namespace Ui {
class MainWindow;
}


namespace Qt3DCore {
class QEntity;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    Qt3DCore::QEntity *rootEntity;
    Qt3DCore::QEntity *tankEntity;
    VechicleInfoPtr vInfo;

    int nationIdx = 0;
    int tagsIdx = 0;
    int levelIdx = 0;

    void setupData();
    void refreshByFilters();
    void load_models();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionPreferences_triggered();

    void on_actionExit_triggered();

    void on_nationsComboBox_currentIndexChanged(int);

    void on_typesComboBox_currentIndexChanged(int);

    void on_levelsComboBox_currentIndexChanged(int);

    void on_tankListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_turret_comboBox_currentIndexChanged(int index);
    void on_pushButton_clicked();
};

#endif // MAINWINDOW_H
