#pragma once
#include "utils/vechicleinfo.h"


class QSqlDatabase;


class TanksDatabase
{
private:
    static QSqlDatabase db;
    static QStringList nations;
    static QStringList levelsInMo;
    static QStringList typesInMo;
    static QStringList typesInListXML;
    static bool load();

public:
    static QStringList translatedNations;
    static QStringList translatedLevels;
    static QStringList translatedTypes;

    TanksDatabase();
    ~TanksDatabase();

    static void init();
    static VechicleInfoPtr loadVechInfo(int tankIdx);
    static QList<QPair<QString, int>> getTranslatedTankListByParams(
            int nationIdx, int tagsIdx, int levelIdx);
};
