#include "xmlsection.h"
#include "xmlparser.h"


bool is_packed(const QByteArray &data)
{
    return *(uint32_t*)data.data() == 0x62a14e45;
}

XMLSection::XMLSection(QByteArray &data)
{
    root_node = XMLNodePtr::create();
    if (is_packed(data)) {
        PackedSection(data, root_node);
    } else {
        QXmlInputSource source;
        source.setData(data);
        QXmlSimpleReader reader;
        XMLParser handler(root_node);
        reader.setContentHandler(&handler);
        reader.parse(source);
    }
}

XMLSection::XMLSection(XMLNodePtr node)
{
    root_node = node;
}

XMLSection::~XMLSection()
{
    //qDebug() << Q_FUNC_INFO;
}

XMLSectionPtr XMLSection::openSection(const QString &path)
{
    QStringList names = path.split("/");

    XMLNodePtr current = root_node;

    for (QString name : names) {
        if (current.isNull())
            return nullptr;
        if (!current->childrens.contains(name))
            return nullptr;
        current = current->childrens.value(name);
    }

    return XMLSectionPtr::create(current);
}

QString XMLSection::readString(const QString &path)
{
    QStringList names = path.split("/");

    XMLNodePtr current = root_node;

    for (QString name : names) {
        if (current.isNull())
            return nullptr;
        if (!current->childrens.contains(name))
            return nullptr;
        current = current->childrens.value(name);
    }

    return current->value;
}

QVector3D XMLSection::readVector3(const QString &path)
{
    QStringList strlist = readString(path).split(" ");

    QVector3D result;

    if (strlist.size() == 3)
        for (int i = 0; i < 3; i++)
            result[i] = strlist.at(i).toFloat();

    return result;
}

QList<QPair<QString, XMLSectionPtr>> XMLSection::items()
{
    QList<QPair<QString, XMLSectionPtr>> _pair_list;

    auto it = root_node->childrens.constKeyValueBegin();
    auto end = root_node->childrens.constKeyValueEnd();

    for (; it != end; it++) {
        QString key = (*it).first;
        XMLNodePtr value = (*it).second;
        _pair_list.append(qMakePair(key, XMLSectionPtr::create(value)));
    }

    return _pair_list;
}

QList<XMLSectionPtr> XMLSection::values()
{
    QList<XMLSectionPtr> _list;

    auto it = root_node->childrens.constBegin();
    auto end = root_node->childrens.constEnd();

    for (; it != end; it++) {
        XMLNodePtr value = *it;
        _list.append(XMLSectionPtr::create(value));
    }

    return _list;
}
