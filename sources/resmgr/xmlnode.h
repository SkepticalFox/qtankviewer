#pragma once
#include "common.h"


struct XMLNode;
typedef QSharedPointer<XMLNode> XMLNodePtr;


struct XMLNode
{
    QString value;
    QWeakPointer<XMLNode> prev;
    QMultiMap<QString, XMLNodePtr> childrens;
};
