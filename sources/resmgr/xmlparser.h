#pragma once
#include "xmlnode.h"
#include <QXmlDefaultHandler>


class XMLParser : public QXmlDefaultHandler
{
private:
    QWeakPointer<XMLNode> m_current_node;
    int level;

public:
    XMLParser(XMLNodePtr root_node);
    ~XMLParser();

    bool startElement(const QString&, const QString&, const QString&, const QXmlAttributes&);
    bool characters(const QString&);
    bool endElement(const QString&, const QString&, const QString&);
};
