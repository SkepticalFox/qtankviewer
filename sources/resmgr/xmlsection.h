#pragma once
#include "packedsection.h"


class XMLSection;
typedef QSharedPointer<XMLSection> XMLSectionPtr;


class XMLSection
{
public:
    XMLNodePtr root_node;

    XMLSection(QByteArray &data);
    XMLSection(XMLNodePtr node);
    ~XMLSection();

    QString readString(const QString &path);
    QVector3D readVector3(const QString &path);
    float readFloat(const QString &path);
    bool readBool(const QString &path);

    XMLSectionPtr openSection(const QString &path);

    QStringList keys();
    QList<XMLSectionPtr> values();
    QList<QPair<QString, XMLSectionPtr> > items();
};
