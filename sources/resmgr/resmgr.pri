include(../libs/miniz/miniz.pri)
include(../libs/ttvfs/ttvfs.pri)

SOURCES += \
    resmgr/resmgr.cpp \
    resmgr/xmlsection.cpp \
    resmgr/packedsection.cpp \
    resmgr/xmlparser.cpp

HEADERS += \
    resmgr/resmgr.h \
    resmgr/xmlsection.h \
    resmgr/packedsection.h \
    resmgr/xmlparser.h \
    resmgr/xmlnode.h
