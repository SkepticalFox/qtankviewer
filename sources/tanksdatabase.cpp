#include "tanksdatabase.h"
#include "resmgr/resmgr.h"
#include "utils/localizationutil.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>


QSqlDatabase TanksDatabase::db;
QStringList TanksDatabase::nations;
QStringList TanksDatabase::levelsInMo;
QStringList TanksDatabase::typesInMo;
QStringList TanksDatabase::typesInListXML;

QStringList TanksDatabase::translatedNations;
QStringList TanksDatabase::translatedLevels;
QStringList TanksDatabase::translatedTypes;


TanksDatabase::TanksDatabase()
{
    qDebug() << Q_FUNC_INFO;
    Q_ASSERT(false);
}

TanksDatabase::~TanksDatabase()
{
    qDebug() << Q_FUNC_INFO;
}

void TanksDatabase::init()
{
    qDebug() << Q_FUNC_INFO;

    if (!QSqlDatabase::isDriverAvailable("QSQLITE"))
    {
        qWarning() << "[ERROR]";
    }

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(":memory:");

    if (!db.open())
    {
        qWarning() << "[ERROR] Database not open";
    }

    QSqlQuery query;
    query.prepare("CREATE TABLE TankList_table ("
                  "id INTEGER PRIMARY KEY NOT NULL, "
                  "tank_nation INTEGER, "
                  "tank_type_id INTEGER, "
                  "tank_name TEXT, "
                  "tank_name_in_res TEXT, "
                  "tank_level INTEGER"
                  ");");

    if (!query.exec())
    {
        qWarning() << "[ERROR]" << query.lastError().text();
        return;
    }

    // text/lc_messages/menu.mo + prefix "nations/"
    nations = QStringList({
        "all",
        "china",
        "czech",
        "france",
        "germany",
        "italy",
        "japan",
        "poland",
        "sweden",
        "uk",
        "usa",
        "ussr"
    });

    // text/lc_messages/menu.mo
    typesInMo = QStringList({
        "#menu:carousel_tank_filter/all",
        "#menu:carousel_tank_filter/lightTank",
        "#menu:carousel_tank_filter/mediumTank",
        "#menu:carousel_tank_filter/heavyTank",
        "#menu:carousel_tank_filter/AT-SPG",
        "#menu:carousel_tank_filter/SPG"
    });

    // for reading tank type from tags in list.xml
    typesInListXML = QStringList({
        "lightTank",
        "mediumTank",
        "heavyTank",
        "AT-SPG",
        "SPG"
    });

    // text/lc_messages/menu.mo
    levelsInMo = QStringList({
        "#menu:levels/all",
        "#menu:levels/1",
        "#menu:levels/2",
        "#menu:levels/3",
        "#menu:levels/4",
        "#menu:levels/5",
        "#menu:levels/6",
        "#menu:levels/7",
        "#menu:levels/8",
        "#menu:levels/9",
        "#menu:levels/10"
    });

    load();
}

bool TanksDatabase::load()
{
    QSqlQuery query;

    LocalizationUtil l10n;

    for (auto nation : nations)
        translatedNations.append(
                    l10n.tr(QString("#menu:nations/%1").arg(nation)));

    for (auto level : levelsInMo)
        translatedLevels.append(l10n.tr(level));

    for (auto type : typesInMo)
        translatedTypes.append(l10n.tr(type));

    for (int i = 1; i < nations.length(); i++) {
        XMLSectionPtr sec = ResMgr::open_section(
                    QString("scripts/item_defs/vehicles/%1/list.xml").arg(nations[i]));
        if (sec.isNull()) {
            qDebug() << nations[i];
            continue;
        }

        for (auto tank_sec : sec->items()) {
            QString tank_name_in_res = tank_sec.first;

            if (tank_name_in_res == "Observer") {
                continue;
            }

            XMLSectionPtr value = tank_sec.second;

            QStringList tags = value->readString("tags").split(" ");

            int tank_type_id = typesInListXML.indexOf(tags.at(0));

            if (tank_type_id == -1) {
                qDebug() << tank_name_in_res << tags;
                continue;
            }

            int tank_level = value->readString("level").toInt();

            QString userString = l10n.tr(value->readString("userString"));

            if (tags.indexOf("secret") != -1)
                userString = "Secret: " + userString;

            query.prepare("INSERT INTO TankList_table"
                          "(tank_nation, tank_type_id, tank_name, tank_name_in_res, tank_level)"
                          "VALUES (?, ?, ?, ?, ?);");

            query.addBindValue(i);
            query.addBindValue(tank_type_id + 1);
            query.addBindValue(userString);
            query.addBindValue(tank_name_in_res);
            query.addBindValue(tank_level);

            if (!query.exec())
            {
                qWarning() << "[ERROR]" << query.lastError().text();
                return false;
            }
        }
    }
    return true;
}

QList<QPair<QString, int>> TanksDatabase::getTranslatedTankListByParams(
        int nationIdx, int tagsIdx, int levelIdx)
{
    QList<QPair<QString, int>> t_list;

    QStringList q_list;

    if (nationIdx)
        q_list.append(QString("tank_nation=%1").arg(nationIdx));

    if (tagsIdx)
        q_list.append(QString("tank_type_id=%1").arg(tagsIdx));

    if (levelIdx)
        q_list.append(QString("tank_level=%1").arg(levelIdx));

    QString query_str = "SELECT tank_name, id FROM TankList_table";
    if (q_list.length())
       query_str += " WHERE " + q_list.join(" AND ");

    QSqlQuery query;

    if (!query.exec(query_str))
        qDebug() << "[ERROR]" << query.lastError().text();

    while (query.next()) {
        t_list.append(qMakePair(query.value(0).toString(), query.value(1).toInt()));
    }

    return t_list;
}

VechicleInfoPtr TanksDatabase::loadVechInfo(int tankIdx)
{
    QSqlQuery query;
    query.prepare("SELECT tank_nation, tank_name_in_res FROM TankList_table WHERE id=?");

    query.addBindValue(tankIdx);

    if (!query.exec()) {
        qDebug() << "[ERROR]" << query.lastError().text();
        return nullptr;
    }

    if (!query.next()) {
        return nullptr;
    }

    int tank_nation = query.value(0).toInt();
    QString tank_name_in_res = query.value(1).toString();

    QString path = QString("scripts/item_defs/vehicles/%1/%2.xml")
            .arg(nations.at(tank_nation))
            .arg(tank_name_in_res);

    VechicleInfoPtr vInfo = VechicleInfoPtr::create(path);

    return vInfo;
}
