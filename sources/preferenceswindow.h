#ifndef PREFERENCESWINDOW_H
#define PREFERENCESWINDOW_H

#include <QDialog>

namespace Ui {
class PreferencesWindow;
}

class PreferencesWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PreferencesWindow(QWidget *parent = 0);
    ~PreferencesWindow();

private slots:
    void on_buttonBox_accepted();

    void on_btnWotPath_clicked();

    void on_btnModPath_clicked();

private:
    Ui::PreferencesWindow *ui;
};

#endif // PREFERENCESWINDOW_H
