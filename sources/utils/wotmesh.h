#pragma once
#include "common.h"


namespace Qt3DRender {
    class QGeometryRenderer;
}


class WoTMesh
{
public:
    QList<Qt3DRender::QGeometryRenderer*> geometryRenderers;

    WoTMesh(const QString &primitives_path, const QString &vres_name, const QString &pres_name);
};
