#include "wotmodel.h"
#include "resmgr/resmgr.h"
#include <Qt3DCore/QTransform>
#include <Qt3DRender/QGeometryRenderer>
#include <Qt3DExtras/QPhongMaterial>


WoTModel::WoTModel(const QString &modelPath,
                   const QVector3D &pos,
                   QEntity *tankEntity) : QEntity(tankEntity)
{
    auto sec = ResMgr::open_section(modelPath);

    if (sec.isNull())
        return;

    QString visualName = sec->readString("nodefullVisual");

    if (visualName.isEmpty())
        visualName = sec->readString("nodelessVisual");

    sec.clear();

    visual = ResMgr::open_section(visualName + ".visual_processed");

    if (visual.isNull())
        return;

    QString primitivesName = visual->readString("primitivesName");

    if (primitivesName.isEmpty())
        primitivesName = visualName;

    auto transform = new Qt3DCore::QTransform;
    transform->setTranslation(pos);
    addComponent(transform);

    for (auto item : visual->items()) {
        if (item.first != "renderSet") {
            continue;
        }
        QString vres_name = item.second->readString("geometry/vertices");
        QString pres_name = item.second->readString("geometry/primitive");
        load_mesh(primitivesName + ".primitives_processed", vres_name, pres_name);
    }

    visual.clear();
}

void WoTModel::load_mesh(const QString &primitives_path, const QString &vres_name, const QString &pres_name)
{
    WoTMesh meshLoader(primitives_path, vres_name, pres_name);

    auto material = new Qt3DExtras::QPhongMaterial(this);
    material->setDiffuse(QColor("red"));

    for (auto geometryRenderer : meshLoader.geometryRenderers) {
        auto modelEntity = new QEntity(this);
        modelEntity->addComponent(geometryRenderer);
        modelEntity->addComponent(material);
    }
}
