#pragma once
#include "common.h"


class Localization_Text
{
public:
    Localization_Text(const QByteArray &data);
    char *text_lookup(const char *s);

private:
    QByteArray mo_ba;
    void *mo_data;
    int reversed;

    int num_strings;
    int original_table_offset;
    int translated_table_offset;
    int hash_num_entries;
    int hash_offset;

    bool label_matches(char *s, int index);
    inline int get_target_index(char *s);
    inline int read4_from_offset(int offset);
    inline char *get_source_string(int index);
    inline char *get_translated_string(int index);
};


typedef QSharedPointer<Localization_Text> Localization_Text_Ptr;
