#pragma once
#include "common.h"


#pragma pack(push)
#pragma pack(1)

struct set3_xyznuviiiwwtbpc_s {
    float xyz[3];
    uint32_t n;
    float uv[2];
    uint8_t iiiww[8];
    uint32_t tb[2];

    static bool is_format(const QByteArray &format)
    {
        return format == "set3/xyznuviiiwwtbpc";
    }
};


struct set3_xyznuvtbpc_s {
  float xyz[3];
  uint32_t n;
  float uv[2];
  uint32_t tb[2];

  static bool is_format(const QByteArray &format)
  {
      return format == "set3/xyznuvtbpc";
  }
};


struct set3_xyznuvpc_s {
  float xyz[3];
  uint32_t n;
  float uv[2];

  static bool is_format(const QByteArray &format)
  {
      return format == "set3/xyznuvpc";
  }
};


struct xyznuviiiwwtb_s {
  float xyz[3];
  uint32_t n;
  float uv[2];
  uint8_t iiiww[5];
  uint32_t tb[2];

  static bool is_format(const QByteArray &format)
  {
      return format == "xyznuviiiwwtb";
  }
};


struct xyznuvtb_s {
  float xyz[3];
  uint32_t n;
  float uv[2];
  uint32_t tb[2];

  static bool is_format(const QByteArray &format)
  {
      return format == "xyznuvtb";
  }
};


struct xyznuv_s {
  float xyz[3];
  float n[3];
  float uv[2];

  static bool is_format(const QByteArray &format)
  {
      return format == "xyznuv";
  }
};

#pragma pack(pop)


float* unpack_normal_old(uint32_t packed);
float* unpack_normal_new(uint32_t packed);
