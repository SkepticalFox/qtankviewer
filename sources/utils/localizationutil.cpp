#include "localizationutil.h"
#include "resmgr/resmgr.h"


LocalizationUtil::LocalizationUtil()
{

}

QString LocalizationUtil::tr(const QString &s)
{
    if (s.at(0) != '#') {
        return s;
    }

    QStringList strlist = s.mid(1).split(":");

    if (strlist.length() != 2) {
        qWarning() << strlist;
    }

    QString mo_filename = strlist.at(0);
    QString mo_str = strlist.at(1);

    if (cache.contains(mo_filename)) {
        return cache[mo_filename]->text_lookup(mo_str.toStdString().c_str());
    }

    QByteArray ba = ResMgr::read_file(QString("text/lc_messages/%1.mo").arg(mo_filename));

    if (ba.isNull()) {
        qWarning() << QString("text/lc_messages/%1.mo").arg(mo_filename);
        return mo_str;
    }

    auto lt = Localization_Text_Ptr::create(ba);

    cache.insert(mo_filename, lt);

    return lt->text_lookup(mo_str.toStdString().c_str());
}
