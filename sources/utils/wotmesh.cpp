#include "wotmesh.h"
#include "resmgr/resmgr.h"
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QGeometry>
#include <Qt3DRender/QGeometryRenderer>
#include <Qt3DRender/QBuffer>
#include "vertextypes.h"


struct PrimitiveGroup {
    uint32_t startIndex;
    uint32_t nPrimitives;
    uint32_t startVertex;
    uint32_t nVertices;
};


WoTMesh::WoTMesh(const QString &primitives_path, const QString &vres_name, const QString &pres_name)
{
    QByteArray verticesBlockData = ResMgr::open_binary(primitives_path, vres_name);
    QBuffer *verticesBlockBuf = new QBuffer(&verticesBlockData);

    if (!verticesBlockBuf || !verticesBlockBuf->open(QBuffer::ReadOnly)) {
        qWarning() << primitives_path << vres_name;
        return;
    }

    QByteArray vertices_subname = verticesBlockBuf->read(64).split('\0')[0];
    QByteArray vertexFormat;


    bool flgNewFormat = false;
    if (vertices_subname.startsWith("BPVT")) {
        verticesBlockBuf->skip(4);
        vertexFormat = verticesBlockBuf->read(64).split('\0')[0];
        flgNewFormat = true;
    }

    uint32_t verticesCount;
    verticesBlockBuf->read((char*)&verticesCount, 4);

    QByteArray sourceVerticesData = verticesBlockBuf->readAll();

    verticesBlockBuf->close();
    delete verticesBlockBuf;
    verticesBlockData = nullptr;


    QByteArray newVertexBufferData;
    newVertexBufferData.resize(verticesCount * 8 * sizeof(float));
    float *rawVertexArray = reinterpret_cast<float *>(newVertexBufferData.data());
    size_t idx = 0;

    if (flgNewFormat) {
        if (set3_xyznuviiiwwtbpc_s::is_format(vertexFormat)) {
            set3_xyznuviiiwwtbpc_s *data = (set3_xyznuviiiwwtbpc_s*)sourceVerticesData.data();
            for (size_t i = 0; i < verticesCount; i++) {
                rawVertexArray[idx++] = data[i].xyz[0];
                rawVertexArray[idx++] = data[i].xyz[1];
                rawVertexArray[idx++] = -data[i].xyz[2];

                float *unpacked = unpack_normal_new(data[i].n);
                rawVertexArray[idx++] = unpacked[0];
                rawVertexArray[idx++] = unpacked[1];
                rawVertexArray[idx++] = -unpacked[2];
                delete[] unpacked;

                rawVertexArray[idx++] = data[i].uv[0];
                rawVertexArray[idx++] = 1 - data[i].uv[1];
            }
        }
        else if (set3_xyznuvtbpc_s::is_format(vertexFormat)) {
            set3_xyznuvtbpc_s *data = (set3_xyznuvtbpc_s*)sourceVerticesData.data();
            for (size_t i = 0; i < verticesCount; i++) {
                rawVertexArray[idx++] = data[i].xyz[0];
                rawVertexArray[idx++] = data[i].xyz[1];
                rawVertexArray[idx++] = data[i].xyz[2];

                float *unpacked = unpack_normal_new(data[i].n);
                rawVertexArray[idx++] = unpacked[0];
                rawVertexArray[idx++] = unpacked[1];
                rawVertexArray[idx++] = unpacked[2];
                delete[] unpacked;

                rawVertexArray[idx++] = data[i].uv[0];
                rawVertexArray[idx++] = 1 - data[i].uv[1];
            }
        }
        else if (set3_xyznuvpc_s::is_format(vertexFormat)) {
            set3_xyznuvpc_s *data = (set3_xyznuvpc_s*)sourceVerticesData.data();
            for (size_t i = 0; i < verticesCount; i++) {
                rawVertexArray[idx++] = data[i].xyz[0];
                rawVertexArray[idx++] = data[i].xyz[1];
                rawVertexArray[idx++] = data[i].xyz[2];

                float *unpacked = unpack_normal_new(data[i].n);
                rawVertexArray[idx++] = unpacked[0];
                rawVertexArray[idx++] = unpacked[1];
                rawVertexArray[idx++] = unpacked[2];
                delete[] unpacked;

                rawVertexArray[idx++] = data[i].uv[0];
                rawVertexArray[idx++] = 1 - data[i].uv[1];
            }
        }
        else {
            qWarning() << "vertexFormat:" << vertexFormat;
            Q_ASSERT(false);
        }
    }
    else {
        if (xyznuviiiwwtb_s::is_format(vertices_subname)) {
            xyznuviiiwwtb_s *data = (xyznuviiiwwtb_s*)sourceVerticesData.data();
            for (size_t i = 0; i < verticesCount; i++) {
                rawVertexArray[idx++] = data[i].xyz[0];
                rawVertexArray[idx++] = data[i].xyz[1];
                rawVertexArray[idx++] = -data[i].xyz[2];

                float *unpacked = unpack_normal_old(data[i].n);
                rawVertexArray[idx++] = unpacked[0];
                rawVertexArray[idx++] = unpacked[1];
                rawVertexArray[idx++] = -unpacked[2];
                delete[] unpacked;

                rawVertexArray[idx++] = data[i].uv[0];
                rawVertexArray[idx++] = 1 - data[i].uv[1];
            }
        }
        else if (xyznuvtb_s::is_format(vertices_subname)) {
            xyznuvtb_s *data = (xyznuvtb_s*)sourceVerticesData.data();
            for (size_t i = 0; i < verticesCount; i++) {
                rawVertexArray[idx++] = data[i].xyz[0];
                rawVertexArray[idx++] = data[i].xyz[1];
                rawVertexArray[idx++] = data[i].xyz[2];

                float *unpacked = unpack_normal_old(data[i].n);
                rawVertexArray[idx++] = unpacked[0];
                rawVertexArray[idx++] = unpacked[1];
                rawVertexArray[idx++] = unpacked[2];
                delete[] unpacked;

                rawVertexArray[idx++] = data[i].uv[0];
                rawVertexArray[idx++] = 1 - data[i].uv[1];
            }
        }
        else if (xyznuv_s::is_format(vertices_subname)) {
            xyznuv_s *data = (xyznuv_s*)sourceVerticesData.data();
            for (size_t i = 0; i < verticesCount; i++) {
                rawVertexArray[idx++] = data[i].xyz[0];
                rawVertexArray[idx++] = data[i].xyz[1];
                rawVertexArray[idx++] = data[i].xyz[2];

                rawVertexArray[idx++] = data[i].n[0];
                rawVertexArray[idx++] = data[i].n[1];
                rawVertexArray[idx++] = data[i].n[2];

                rawVertexArray[idx++] = data[i].uv[0];
                rawVertexArray[idx++] = 1 - data[i].uv[1];
            }
        }
        else {
            qWarning() << "vertices_subname:" << vertices_subname;
            qWarning() << "vertexFormat:" << vertexFormat;
            Q_ASSERT(false);
        }
    }


    auto customGeometry = new Qt3DRender::QGeometry;
    auto vertexDataBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, customGeometry);
    auto indexDataBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer, customGeometry);

    vertexDataBuffer->setData(newVertexBufferData);


    // indices
    QByteArray primitivesBlockData = ResMgr::open_binary(primitives_path, pres_name);
    QBuffer *primitivesBlockBuf = new QBuffer(&primitivesBlockData);

    if (!primitivesBlockBuf || !primitivesBlockBuf->open(QBuffer::ReadOnly)) {
        qWarning() << primitives_path << pres_name;
        return;
    }

    QByteArray indexFormat = primitivesBlockBuf->read(64).split('\0')[0];

    uint32_t nIndices;
    uint32_t nTriangleGroups;

    primitivesBlockBuf->read((char*)&nIndices, 4);
    primitivesBlockBuf->read((char*)&nTriangleGroups, 4);

    QByteArray indexBufferData;

    Qt3DRender::QAttribute::VertexBaseType triType;
    if (indexFormat == "list") {
        triType = Qt3DRender::QAttribute::UnsignedShort;
        indexBufferData = primitivesBlockBuf->read(nIndices * sizeof(uint16_t));
    }
    else if (indexFormat == "list32") {
        triType = Qt3DRender::QAttribute::UnsignedInt;
        indexBufferData = primitivesBlockBuf->read(nIndices * sizeof(uint32_t));
    }

    indexDataBuffer->setData(indexBufferData);

    PrimitiveGroup *primitiveGroups = new PrimitiveGroup[nTriangleGroups];
    primitivesBlockBuf->read((char*)primitiveGroups, nTriangleGroups*sizeof(PrimitiveGroup));

    primitivesBlockBuf->close();
    delete primitivesBlockBuf;
    primitivesBlockData = nullptr;


    // attributes
    auto positionAttribute = new Qt3DRender::QAttribute;
    positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    positionAttribute->setBuffer(vertexDataBuffer);
    positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    positionAttribute->setVertexSize(3);
    positionAttribute->setByteOffset(0);
    positionAttribute->setByteStride(8 * sizeof(float));
    positionAttribute->setCount(verticesCount);
    positionAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
    //positionAttribute->setName("iVertPos");

    Qt3DRender::QAttribute *normalAttribute = new Qt3DRender::QAttribute();
    normalAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    normalAttribute->setBuffer(vertexDataBuffer);
    normalAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    normalAttribute->setVertexSize(3);
    normalAttribute->setByteOffset(3 * sizeof(float));
    normalAttribute->setByteStride(8 * sizeof(float));
    normalAttribute->setCount(4);
    normalAttribute->setName(Qt3DRender::QAttribute::defaultNormalAttributeName());
    //normalAttribute->setName("iNormal");

    auto textureUVAttribute = new Qt3DRender::QAttribute;
    textureUVAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    textureUVAttribute->setBuffer(vertexDataBuffer);
    textureUVAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    textureUVAttribute->setVertexSize(2);
    textureUVAttribute->setByteOffset(6 * sizeof(float));
    textureUVAttribute->setByteStride(8 * sizeof(float));
    textureUVAttribute->setCount(verticesCount);
    textureUVAttribute->setName(Qt3DRender::QAttribute::defaultTextureCoordinateAttributeName());
    //textureUVAttribute->setName("iTexCoord1");

    auto indexAttribute = new Qt3DRender::QAttribute;
    indexAttribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    indexAttribute->setBuffer(indexDataBuffer);
    indexAttribute->setVertexBaseType(triType);
    indexAttribute->setVertexSize(1);
    indexAttribute->setByteOffset(0);
    indexAttribute->setByteStride(0);
    indexAttribute->setCount(nIndices);

    customGeometry->addAttribute(positionAttribute);
    customGeometry->addAttribute(normalAttribute);
    customGeometry->addAttribute(textureUVAttribute);
    customGeometry->addAttribute(indexAttribute);

    for (size_t i = 0; i < nTriangleGroups; i++) {
        auto geometryRenderer = new Qt3DRender::QGeometryRenderer;

        geometryRenderer->setFirstVertex(primitiveGroups[i].startIndex);
        geometryRenderer->setVertexCount(primitiveGroups[i].nPrimitives * 3);
        geometryRenderer->setPrimitiveType(Qt3DRender::QGeometryRenderer::Triangles);
        geometryRenderer->setGeometry(customGeometry);

        geometryRenderers.append(geometryRenderer);
    }

    delete[] primitiveGroups;
}
