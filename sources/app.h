#pragma once
#include "common.h"


class TankViewerApp
{
private:
    static QString _wotPath;
    static QString _modPath;

    static void readSettings();
    static void writeSettings();

public:
    static void init();
    static void fini();

    static const QString wotPath() { return _wotPath; }
    static const QString modPath() { return _modPath; }

    static void setWotPath(const QString &path) { _wotPath = path; }
    static void setModPath(const QString &path) { _modPath = path; }
};
