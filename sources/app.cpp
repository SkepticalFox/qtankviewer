#include "app.h"
#include "resmgr/resmgr.h"
#include "tanksdatabase.h"

#include <QXmlStreamReader>
#include <QFile>
#include <QSettings>


QString TankViewerApp::_wotPath;
QString TankViewerApp::_modPath;


static void mountWotResources()
{
    QFile file("../data/resources.xml");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QStringList pkgs;
    QStringList dirs;

    QXmlStreamReader xml(&file);
    while (!xml.atEnd() && !xml.hasError()) {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token != QXmlStreamReader::StartElement)
            continue;
        if (xml.name() == "Pkg") {
            xml.readNext();
            QString path = xml.text().toString();
            QFile pkgPath(TankViewerApp::wotPath() + "\\res\\packages\\" + path);
            if (!pkgPath.exists())
                continue;
            pkgs.append(pkgPath.fileName());
        } else if (xml.name() == "Dir") {
            xml.readNext();
            QString dirPath = xml.text().toString();
            dirs.append(dirPath);
        }
    }
    file.close();

    // dirty hack
    if (!TankViewerApp::modPath().isEmpty()) {
        dirs.append(TankViewerApp::modPath());
    }

    // dirty hack
    if (!TankViewerApp::wotPath().isEmpty()) {
        dirs.append(TankViewerApp::wotPath()  + "/res");
    }

    ResMgr::mount_dirs(dirs);
    ResMgr::mount_pkgs(pkgs);
}


void TankViewerApp::readSettings()
{
    //qDebug() << Q_FUNC_INFO;

    QSettings settings("../data/settings.ini", QSettings::IniFormat);
    settings.beginGroup("Paths");
    _wotPath = settings.value("WotPath", "C:\\Games\\World_of_Tanks").toString();
    _modPath = settings.value("ModPath", "").toString();
    settings.endGroup();
}

void TankViewerApp::writeSettings()
{
    //qDebug() << Q_FUNC_INFO;

    QSettings settings("../data/settings.ini", QSettings::IniFormat);
    settings.beginGroup("Paths");
    settings.setValue("WotPath", _wotPath);
    settings.setValue("ModPath", _modPath);
    settings.endGroup();
}

void TankViewerApp::init()
{
    qDebug() << Q_FUNC_INFO;

    readSettings();

    ResMgr::init();

    mountWotResources();

    TanksDatabase::init();
}

void TankViewerApp::fini()
{
    qDebug() << Q_FUNC_INFO;

    ResMgr::fini();

    writeSettings();
}
