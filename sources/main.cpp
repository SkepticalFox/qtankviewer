#include <QtWidgets/QApplication>
#include "app.h"
#include "mainwindow.h"

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    TankViewerApp::init();

    MainWindow w;
    w.show();

    int result = a.exec();

    TankViewerApp::fini();

    return result;
}
